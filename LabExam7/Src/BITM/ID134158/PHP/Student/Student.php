<?php
namespace App\BITM\ID134158\PHP\Student;

use PDO;

class Student
{
    public $id = '';
    public $first_name = '';
    public $last_name = '';
    public $nickname = '';
    public $conn = '';
    public $user = '';
    public $pass = '';
    public $dbname = 'LabExam7';
    public $host = 'localhost';

    public function __construct()
    {
        try {
            $this->conn = new PDO("mysql:host = $this->host; dbname = $this->dbname;", $this->user, $this->pass);

        } catch
        (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($d = '')
    {
        if (array_key_exists('first_name', $d) && !empty($d)) {
            $this->first_name = $d['first_name'];
        }
        if (array_key_exists('last_name', $d) && !empty($d)) {
            $this->last_name = $d['last_name'];
        }
        if (array_key_exists('nickname', $d) && !empty($d)) {
            $this->nickname = $d['nickname'];
        }

        return $this;

    }

    public function store()
    {
        $qstore = "INSERT INTO student (`first_name`,`last_name`,`nickname`) VALUES (:first_name,:last_name,:nickname)";
        $statement = $this->conn->prepare($qstore);
        $get = $statement->execute(array(':first_name' => $this->first_name, ':last_name' => $this->last_name, ':nickname' => $this->nickname));
       // echo $get;
    }

}
