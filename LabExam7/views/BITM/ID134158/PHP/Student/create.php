<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Creat Function</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
              integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
                crossorigin="anonymous"></script>
    </head>
    <body>
        <form action="store.php" method="post">
            <div class="form-group col-md-10 col-md-offset-1">
                <label for="first_name">First Name</label>
                <input type="text" class="form-control" id="first_name" name="first_name"placeholder="Your First Name">
            </div>
            <div class="form-group col-md-10 col-md-offset-1">
                <label for="last_name">Last Name</label>
                <input type="text" class="form-control" id="last_name" name="last_name"placeholder="Your Last Name">
            </div>
            <div class="form-group col-md-10 col-md-offset-1">
                <label for="nickname">Nickname</label>
                <input type="text" class="form-control" id="nickname" name="nickname"placeholder="Your Nickname">
            </div>


            <button type="submit" class="btn btn-default col-md-offset-1">Submit</button>
        </form>
    </body>
</html>