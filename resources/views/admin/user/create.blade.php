@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                    {!! Form::open(array('url' => 'user/store', 'method'=>'post' )) !!}
                    <div class="form-group">
                        {!! Form::label('first_name') !!}
                        {!! Form::text('name', null, ['class'=> 'form-control', 'placeholder' => 'Ex:John']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('last_name') !!}
                        {!! Form::text('name', null, ['class'=> 'form-control', 'placeholder' => 'Ex:John']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Gender') !!}
                        {!! Form::select('gender', ['male' => 'Male', 'female'=> 'Female']) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('father_name') !!}
                    {!! Form::text('name', null, ['class'=> 'form-control', 'placeholder' => 'Ex:John']) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('mother_name') !!}
                    {!! Form::text('name', null, ['class'=> 'form-control', 'placeholder' => 'Ex:John']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('mobile') !!}
                        {!! Form::text('mobile', null, ['class'=> 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('password') !!}
                        {!! Form::password('password', ['class'=> 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('address') !!}
                        {!! Form::textarea('address', null, ['class'=> 'form-control']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('facebook') !!}
                        {!! Form::url('facebook', null, ['class'=> 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('website') !!}
                        {!! Form::url('website', null, ['class'=> 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('git') !!}
                        {!! Form::url('git', null, ['class'=> 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('linkedin') !!}
                        {!! Form::url('linkedin', null, ['class'=> 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Submit', null, ['class'=> 'form-control']) !!}
                    </div>
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
