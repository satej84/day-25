<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'Gender','father_name', 'mother_name', 'mobile','address', 'facebook', 'website',
        'git', 'linkedin',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
