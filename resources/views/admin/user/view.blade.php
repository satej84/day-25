@extends('layouts.app')

@section('content')
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    <table border="1" class="table table-bordered table-responsive">
                        <tr>
                            <th>first_name</th>
                            <th>{{$users->profile->first_name}}</th>
                        </tr>
                        <tr>
                            <th>last_name</th>
                            <th>{{$users->profile->last_name}}</th>
                        </tr>
                        <tr>
                            <th>Gender</th>
                            <th>{{$users->profile->Gender}}</th>
                        </tr>
                        <tr>
                            <th>father_name</th>
                            <th>{{$users->profile->father_name}}</th>
                        </tr>
                        <tr>
                            <th>mother_name</th>
                            < <th>{{$users->profile->mother_name}}</th>
                        </tr>
                        <tr>
                            <th>mobile</th>
                            <th>{{$users->profile->mobile}}</th>
                        </tr>
                        <tr>
                            <th>address</th>
                            <th>{{$users->profile->address}}</th>
                        </tr>
                        <tr>
                            <th>facebook</th>
                            <th>{{$users->profile->facebook}}</th>
                        </tr>
                        <tr>
                            <th>website</th>
                            <th>{{$users->profile->website}}</th>
                        </tr>
                        <tr>
                            <th>git</th>
                            <th>{{$users->profile->git}}</th>
                        </tr>
                        <tr>
                            <th>linkedin</th>
                            <th>{{$users->profile->linkedin}}</th>
                        </tr>
                    </table>

                </div>
@endsection