@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard |
                        <a href="{{url('/user/add')}}">Add New User</a>
                    </div>
                       <table border="1" class="table table-bordered table-responsive">
                           <thead>
                            <tr>
                                <td>Id</td>
                                <td>Name</td>
                                <td>Email</td>
                                <td>Action</td>
                            </tr>
                           </thead>
                           <thead>
                           @foreach($user as $users)
                               <tr>
                               <td>{{$users->id}}</td>
                               <td>{{$users->name}}</td>
                               <td>{{$users->email}}</td>
                                   <td>
                                       <a href="{{url('/user/'.$users->id)}}"> View |</a> Edit | Delete</td>
                               </tr>
                           @endforeach
                           </thead>
                       </table>

                </div>
             </div>
        </div>
    </div>
@endsection