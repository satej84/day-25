<?php
$factory->define(App\Profile::class, function (Faker\Generator $faker) {

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'Gender' => $faker->randomElement($array=array('male','female')),
        'father_name' => $faker->name,
        'mother_name' => $faker->name,
        'mobile' => $faker->phoneNumber,
        'address' => $faker->address,
        'facebook' =>'http;//www.facebook.com/'. $faker->unique()->firstName,
        'website' => $faker->url,
        'git' => 'http;//www.github.com/'. $faker->unique()->firstName,
        'linkedin' => 'http;//www.linkedin.com/'. $faker->unique()->firstName,
    ];
});
